//
//  ViewRadius.swift
//  Speedworks Courier
//
//  Created by Henry Bryan Gasga on 8/28/20.
//  Copyright © 2020 Speedworks Courier Services Corporation. All rights reserved.
//

import UIKit

class ViewRadius: UIView {
    @IBInspectable
    public var BorderWidth: CGFloat = 1.0 {
        didSet {
            self.layer.borderWidth = self.BorderWidth
        }
    }
    
    @IBInspectable
    public var BorderColor: UIColor {
        get {
            return UIColor(cgColor: self.layer.borderColor!)
        }
        set {
            self.layer.borderColor = newValue.cgColor
        }
    }
    
    override func draw(_ rect: CGRect) {
        super.draw(rect)
        //  self.createGradientLayer()
        self.layer.borderWidth = 0.0
       // self.layer.borderColor = UIColor.gray.cgColor
        self.layer.cornerRadius = 10
//        self.layer.cornerRadius = self.frame.size.height/2
        
//        self.layer.shadowColor = UIColor.black.cgColor
//        self.layer.masksToBounds = false
//        self.layer.shadowOpacity = 8
//        self.layer.shadowOffset = .zero
//        self.layer.shadowRadius = 5
//        self.layer.shadowPath = UIBezierPath(rect: self.bounds).cgPath
//        self.layer.shouldRasterize = true
//        self.layer.rasterizationScale = UIScreen.main.scale
        
        self.clipsToBounds = true
    }
    

}
