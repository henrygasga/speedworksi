//
//  ViewController.swift
//  Speedworks Courier
//
//  Created by Henry Bryan Gasga on 8/27/20.
//  Copyright © 2020 Speedworks Courier Services Corporation. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    
    @IBOutlet weak var searchBar: ViewRadius!
    @IBOutlet weak var searchshadow: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        searchBar.layer.masksToBounds = true
        searchshadow.layer.cornerRadius = 10
        searchshadow.layer.shadowColor = UIColor.black.cgColor
        searchshadow.layer.shadowOpacity = 0.2
        searchshadow.layer.shadowOffset = CGSize(width: 0.5, height: 0.5)
        searchshadow.layer.shadowRadius = 5
        searchshadow.addSubview(searchBar)
    }


}

